'use strict';

const divelib = require('./dive'),
	mongoose = require('mongoose'),
	log = require('khem-log'),
	fs = require('fs'),
	kerror = require('khem-error');

mongoose.Promise = require('bluebird');

const parseError = (type, error, doc, next) => {
	error = kerror.generator(error);
	error.doc = doc;
	error.method = type;
	error.file = 'khem-mongoose';
	return next(error);
};

const odm = {
	mongoose,
	db: mongoose.connection,
	modelsData: {},
	models: {},
	authenticate: [],

	__error: {
		save: (error, doc, next) => {
			return parseError('save', error, doc, next);
		},
		update: (error, doc, next) => {
			return parseError('update', error, doc, next);
		},
		remove: (error, doc, next) => {
			return parseError('remove', error, doc, next);
		}
	},

	connect(config) {
		const connection = `mongodb://${config.HOST}:${config.PORT}/${config.NAME}`;
		log.info(`[node_modules/khem-mongoose/index.js, connect] Connecting to ${connection}`);
		return mongoose.connect(connection, { promiseLibrary: require('bluebird') });
	},

	__loadMiddlewares(schema, type, middlewares) {
		const keys = Object.keys(middlewares);
		keys.forEach(middleware => {
			schema[type](middleware, middlewares[middleware]);
		});
	},

	__loadPre(schema, middlewares) {
		this.__loadMiddlewares(schema, 'pre', middlewares);
	},

	__loadPost(schema, middlewares) {
		this.__loadMiddlewares(schema, 'post', middlewares);
	},

	__loadError(name, schema, middlewares) {
		if (!middlewares.save) {
			middlewares.save = this.__error.save;
		}
		if (!middlewares.update) {
			middlewares.update = this.__error.update;
		}
		if (!middlewares.remove) {
			middlewares.remove = this.__error.remove;
		}
		this.__loadMiddlewares(schema, 'post', middlewares);
	},

	__loadErrorDefault(name, schema) {
		this.__loadMiddlewares(schema, 'post', this.__error);
	},

	dive() {
		// Recurse through the api directory and collect the models
		const dirname = __dirname.replace('node_modules/khem-mongoose', 'server/');
		const path_models = `${dirname}models/mongoose/`;
		const path_middlewares = `${dirname}middlewares/mongoose`;
		divelib(path_models, (err, path) => {
			let modelname = path.substring(path.lastIndexOf('/') + 1, path.indexOf('.model.js'));
			log.info(`Loading (mongodb) Model: ${modelname} from ${modelname}.model.js`);
			try {
				let data = require(path);
				let middlewareName = `${modelname}.middleware.js`;
				// Require middlewares
				if (fs.existsSync(`${path_middlewares}/${middlewareName}`)) {
					log.info(`Loading (mongodb) middleware: ${modelname} from ${middlewareName}`);
					let middlewares = require(`${path_middlewares}/${middlewareName}`);
					if (middlewares.pre) this.__loadPre(data.schema, middlewares.pre);
					if (middlewares.post) this.__loadPost(data.schema, middlewares.post);
					if (middlewares.error) this.__loadError(modelname, data.schema, middlewares.error);
					else this.__loadErrorDefault(modelname, data.schema);
				} else {
					this.__loadErrorDefault(modelname, data.schema);
				}
				// Create model from schema
				this.modelsData[modelname] = {
					config: data.configuration.config,
					authenticate: data.configuration.authenticate,
					schema: mongoose.model(modelname, data.schema)
				};
				this.models[modelname] = this.modelsData[modelname].schema;
			} catch (err) {
				log.error(err, 'node_modules/khem-mongoose/index.js', 'dive');
			}
		});
	},

	load(config) {
		return this.connect(config).then(() => this.dive());
	}
};

module.exports = odm;
