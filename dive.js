const fs = require('fs');
const log = require('khem-log');

// Recurse through the specified directory structure
const dive = function(dir, callback) {
	// Assert that it's a function
	if (typeof callback !== 'function') callback = function(error, file) {};
	// Read the directory
	fs.readdirSync(dir).forEach(file => {
		var path = dir + file; // Full path of that file
		var stat = fs.statSync(path); // Get the file's stats
		// If the file is a directory
		if (stat && stat.isDirectory()) dive(path, callback);
		// Dive into the directory
		else {
			if (file.indexOf('.') !== 0 && file.indexOf('.model.js') > 0) callback(null, path);
			// Call the action
			else
				log.warning(
					`[node_modules/khem-mongoose/dive.js, dive] Incorrect file name found on mongodb models path: ${file}`
				);
		}
	});
};

module.exports = dive;
